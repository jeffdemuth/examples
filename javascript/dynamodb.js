AWS.config.update({
  region: "us-east-1",
  endpoint: 'https://dynamodb.us-east-1.amazonaws.com',
  accessKeyId: "",
  secretAccessKey: ""
});

var docClient = new AWS.DynamoDB.DocumentClient();

function fnReadItem() {
    var table = "tblHTML";
    var title = "header";

    var params = {
        TableName: table,
        Key:{
            "pk": title
        }
    };
    docClient.get(params, function(err, data) {
        if (err) {
            document.getElementById('header').innerHTML = "Unable to read item: " + "\n" + JSON.stringify(err, undefined, 2);
        } else {
            document.getElementById('header').innerHTML = data.Item.code;
        }
    });
}