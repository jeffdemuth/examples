import json
import paho
import boto3
import paho.mqtt.client as mqtt
import ssl
import time
import os
import datetime
endpoint = os.environ['iotendpoint']

ssmClient = boto3.client('ssm')
iotca = ssmClient.get_parameter(Name='iotrootca',WithDecryption=False)
file = open('/tmp/root.ca.pem','w')
file.write(iotca['Parameter']['Value'])
file.close() 
iotcert = ssmClient.get_parameter(Name='iotcert',WithDecryption=False)
file = open('/tmp/certificate.ca.crt','w')
file.write(iotcert['Parameter']['Value'])
file.close() 
iotkey = ssmClient.get_parameter(Name='iotkey',WithDecryption=True)
file = open('/tmp/private.pem.key','w')
file.write(iotkey['Parameter']['Value'])
file.close() 

def lambda_handler(event, context):
    client = mqtt.Client()
    ssl_context = ssl.create_default_context()
    ssl_context.set_alpn_protocols(['x-amzn-mqtt-ca'])
    ssl_context.load_verify_locations(cafile='/tmp/root.ca.pem')
    ssl_context.load_cert_chain(certfile='/tmp/certificate.ca.crt', keyfile='/tmp/private.pem.key')
    client.tls_set_context(context=ssl_context)
    client.connect(endpoint, 443, 60)
    client.loop_start()
    while True:
        now = datetime.datetime.now()
        client.publish('hello/world', '{"Current Time":"' + str(now) + '"}')
        time.sleep(5)
